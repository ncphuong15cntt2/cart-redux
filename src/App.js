import React, { PureComponent } from 'react'
import Header from './components/Header'
import Products from './components/Products'
import Notification from './components/Notification'
import Cart from './components/Cart'
import Footer from './components/Footer'

class App extends PureComponent {
  render() {
    return (
      <div>
        {/* Header */}
        <Header />
        <main id="mainContainer">
          <div className="container">
            {/* Products */}
            <Products />
            {/* Message */}
            <Notification />
            {/* Cart */}
            <Cart />
          </div>
        </main>
        {/* Footer */}
        <Footer />
      </div>
    )
  }
}

export default App
