import { combineReducers } from 'redux'
import products from './products'

const myReducers = combineReducers({
  products,
})

export default myReducers
