import React, { PureComponent } from 'react'
import ProductItem from './ProductItem'

class Products extends PureComponent {
  render() {
    return (
      <section className="section">
        <h1 className="section-heading">Danh Sách Sản Phẩm</h1>
        <div className="row">
          <ProductItem />
          <ProductItem />
          <ProductItem />
        </div>
      </section>
    )
  }
}

export default Products
