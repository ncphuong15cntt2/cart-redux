import React, { PureComponent } from 'react'

class Notification extends PureComponent {
  render() {
    return (
      <h3>
        <span className="badge amber darken-2">Mua Hàng Thành Công !</span>
      </h3>
    )
  }
}

export default Notification
